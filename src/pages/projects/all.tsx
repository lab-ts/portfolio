import {
  Box,
  VStack,
  Button,
  Flex,
  Divider,
  chakra,
  Grid,
  GridItem,
  Container,
} from '@chakra-ui/react';

interface FeatureProps {
  heading: string;
  text: string;
  url?: string;
}

const Feature = ({ heading, text, url }: FeatureProps) => {
  return (
    <GridItem>
      <chakra.h3 fontSize="xl" fontWeight="600">
        {heading}
      </chakra.h3>
      <chakra.p>{text}</chakra.p>
      {url ? <chakra.a href={url} target={"_blank"} color={"#6CACE4"}> Acessar projeto </chakra.a> : ''}
    </GridItem>
  );
};

export default function gridListWithCTA() {
  return (
    <Box as={Container} maxW="7xl" mt={14} p={4}>
      <Grid
        templateColumns={{
          base: 'repeat(1, 1fr)',
          sm: 'repeat(2, 1fr)',
          md: 'repeat(2, 1fr)',
        }}
        gap={4}>
        <GridItem colSpan={1}>
          <VStack alignItems="flex-start" spacing="20px">
            <chakra.h2 fontSize="3xl" fontWeight="700">
              Lista de projetos
            </chakra.h2>
            <Button colorScheme="blue" size="md">
              Ultimo projeto
            </Button>
          </VStack>
        </GridItem>
        <GridItem>
          <Flex>
            <chakra.p>
              Aqui você encontra a lista de alguns dos projetos realizados em estudos, trabalhos e voluntáriados. 
              fique a vontade para olhá-los e caso tenha interesse entre em contato comigo e tire sua dúvida.
            </chakra.p>
          </Flex>
        </GridItem>
      </Grid>
      <Divider mt={12} mb={12} />
      <Grid
        templateColumns={{
          base: 'repeat(1, 1fr)',
          sm: 'repeat(2, 1fr)',
          md: 'repeat(4, 1fr)',
        }}
        gap={{ base: '8', sm: '12', md: '16' }}>
          <Feature
          heading={'Gateway skeleton'}
          text={'A node API gateway Skeleton with manager proxy.'}
          url={'https://gitlab.com/Tonybsilva-dev/api-gateway-skeleton'}
        />
        <Feature
          heading={'tonybsilva-ui-kit'}
          text={'Simple ui kit library for React.js with storybook demo.'}
          url={'https://github.com/Tonybsilva-dev/tonybsilva-ui-kit'}
        />
        <Feature
          heading={'Pokedex'}
          text={'A simple Pokedex in React.js.'}
          url={'https://github.com/Tonybsilva-dev/Pokedex'}
        />
        <Feature
          heading={'Net Promoter Score API'}
          text={'A simple API to NPS feature with send mail.'}
        />
        <Feature
          heading={'Go Barber'}
          text={'A complete React BarberApp Application.'}
        />
        <Feature
          heading={'DotDoc'}
          text={'A Notion alternative written with Electron.'}
          url={'https://gitlab.com/lab-ts/dotdoc'}
        />
        <Feature
          heading={'Embryo'}
          text={'A personal dashboard concept.'}
          url={'https://github.com/Tonybsilva-dev/embryo'}
        />
        <Feature
          heading={'Feedget'}
          text={'A feedback widget to improve user experience.'}
          url={"https://gitlab.com/TS-open-source/feedback-widget"}
        />
        <Feature
          heading={'Social Heat'}
          text={'A real-time chat experience.'}
          url={'https://gitlab.com/TS-open-source/SocialHeat'}
        />
        <Feature
          heading={'Let me ask'}
          text={'All question has an answer.'}
        />
        <Feature
          heading={'Corona Data'}
          text={'COVID-19 Tracker Application.'}
          url={'https://github.com/Tonybsilva-dev/CoronaData'}
        />
        <Feature
          heading={'Move.It'}
          text={'improve your productivity and focus.'}
        />
        <Feature
          heading={'Ecoleta'}
          text={'Your waste collection marketplace.'}
          url={"https://github.com/Tonybsilva-dev/Ecoleta"}
        />
        <Feature
          heading={'Radar Dev'}
          text={'An application that facilitates the search for developers in your region.'}
          url={'https://github.com/Tonybsilva-dev/DevRadar'}
        />
      </Grid>
    </Box>
  );
}